from configparser import ConfigParser
from pyrogram import Client, Filters

config = ConfigParser()
config.read("config.ini")


@Client.on_message(Filters.regex(config["trigger"]["delete_my_messages"]) & Filters.me)
def delete_my_messages(client, message):
    for msg in client.iter_history(message.chat.id):
        if msg.from_user.id == message.from_user.id:
            msg.delete()


@Client.on_message(Filters.regex(config["trigger"]["delete_all_messages"]) & Filters.me)
def delete_all_messages(client, message):
    for msg in client.iter_history(message.chat.id):
        msg.delete()
