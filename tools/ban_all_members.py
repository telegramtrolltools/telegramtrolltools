import time
from configparser import ConfigParser
from pyrogram import Client, Filters
from pyrogram.errors import FloodWait

config = ConfigParser()
config.read("config.ini")


@Client.on_message(Filters.regex(config["trigger"]["ban_all_members"]) & Filters.me)
def kick_all(client, message):
    message.delete()
    channel = message.chat
    for i, member in enumerate(client.iter_chat_members(channel.id)):
        try:
            if member.status in ("creator", "administrator", "kicked", "left"):
                print(
                    f"- Skipping {member.status} {member.user.first_name} (user #{i})"
                )
                continue
            print(f"x Kicking {member.status} {member.user.first_name} (user #{i})")
            msg = channel.kick_member(member.user.id)
            try:
                msg.delete()
            except AttributeError:
                pass
            time.sleep(1)
        except FloodWait as e:
            print(f"  Waiting for {e.x} seconds...")
            time.sleep(e.x)  # Wait "x" seconds before continuing
