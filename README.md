# Telegram Troll Tools

Tools um Verschwörungstheorien/Nazi Gruppen zu Trollen oder zerstören.

## Installation
```
git clone https://gitlab.com/telegramtrolltools/telegramtrolltools.git
pip install pyrogram
cp config.ini.example config.ini
nano config.ini
```
Replace the api id and api hash with yours. And if you want, change the trigger for the functions.

## Usage
just run it with:
```
python bot.py
```
The first time you run it, you need to type your phone number and a confirmation code.
Then you can use the commands.
### Functions

#### `ban_all_members`
Bans all members of the chat, where you send the trigger, works only if you have the permission to ban users.

#### `delete_all_messages`
Deletes all messages of the chat, where you send the trigger, works only if you have the permission to delete messages.

#### `delete_my_messages`
Deletes all your messages in the chat, you send the trigger, works always